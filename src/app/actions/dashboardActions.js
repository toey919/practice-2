import axios from 'axios';
import { SET_CAMPAIGNS_LIST,SET_AUDIENCE_LIST,SET_SURVEY_DASHBOARD_LIST } from './types';

export function setCampaignsList(campaigns) {
  return {
    type: SET_CAMPAIGNS_LIST,
    campaigns
  }
}

export function getCampaignsList() {
  return dispatch => {
     return axios.get('jsons/campaigns.json').then(function(result){ 
          dispatch(setCampaignsList(result));         
    });
  }
}

export function setAudienceList(audience) {
  return {
    type: SET_AUDIENCE_LIST,
    audience
  }
}

export function getAudienceList() {
  return dispatch => {
    return axios.get('jsons/audience.json').then(function(result){ 
      dispatch(setAudienceList(result));     
    });
  }
}

export function setsurveyDashboardList(survey) {
  return {
    type: SET_SURVEY_DASHBOARD_LIST,
    survey
  }
}

export function getsurveyDashboardList() {
  return dispatch => {
    return axios.get('jsons/surveyDashboard.json').then(function(result){ 
      dispatch(setsurveyDashboardList(result));     
    });
  }
}